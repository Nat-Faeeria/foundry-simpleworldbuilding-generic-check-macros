# Modulable macros for simple checks with Simple World Building on Foundry

## Goal

Those simple macros help you set-up automated checks for your custom game system build with Simple World Building.

## How To Use ?

1. Create a world with Simple World Building
2. Create actors using the default character sheet (should work with others too)
3. In your actors, add attributes by creating Attribute Groups, then adding your characteristics in those categories.
Everything as been tested with Number types for the characteristics

![](images/macro-template-actor.png)

4. Add a macro using the quick macro bar : click on any empty box and a new macro will be created, and the macro editor 
be opened

![](images/macro-bar.png)

![](images/macro-display.png)

5. In an editor, copy the `checkmacro-template.js` then modify this part :
```
const macroParameters = {
	diceToRoll: "d20",
	category: "skills",
	checkName: "Skill",
	checkFunction: (roll, DC) => roll>=DC,
	critRolls: {
		enabled: true, 
		success: (roll, total, DC) => roll==20 || total >= DC+10,
		failure: (roll, total, DC) => roll==1 || total<=DC-10
	},
	DC: 0,
	specifyDC: false,
	bonusToRoll: true,
	bonusFunction: "+"
}
```
  - `diceToRoll` is the number and type of dice you want to roll for the check
  - `category` is the Attribute Group you want to base your checks on
  - `checkName` is the name you want to give to this check (on the displays)
  - `checkFunction` is the function you want to use for the roll. Default is D20 system,
  comparing if the roll is greater than or equal to a Difficulty Class. You can easily change the function
  by changing the `roll>=DC` part : `roll` is the result from your roll and `DC` is the Difficulty Class
  - `critRolls` allows you to define a behavior for critical rolls :
    - `enabled` enables or disables crit rolls (values are `true` or `false`)
    - `success` is a function that determines if a roll is a crit success or not (default is set for a system like PF2 : 
    nat 20 or 10 higher than DC)
    - `failure` is a function that determines if a roll is a crit failure or not (default is set for a system like PF2 : 
    nat 1 or 10 lower than DC)

      Both of those functions are meant to be use with the variable `origRoll`, which is the roll without any mods
  - `DC` set the Difficulty Class you want to test against
  - `specifyDC` allows you to display an input to specify the check DC **Does not work if the DC parameter is not 0 !**
  - `bonusToRoll` is a simple boolean if you want to add the possibility of a bonus or malus to your rolls : 
  give it the value `true` if you want it, `false` if you don't
  - `bonusFunction` is the way you want your bonus applied if you do have a bonus. Two choices are currently 
  supported : `"+"` if you want the bonus to be added to your roll, `"-"` if you want the bonus to be substracted 
  from your roll

6. Copy the whole macro and paste it in the Macro Editor
7. Select the `script` type for your macro
8. (optional) Add an icon to your macro by clicking on the icon section of the Editor

Now your macro should look like that : 

![](images/macro-completed.png)

9. Place a token from one of your actors in a Scene, then select it
10. Launch the macro and let the magic happen !

![](images/macro-check-display.png)

![](images/macro-check-results.png)

## Changing the display of results

You can modify the result's text and color by changing those lines :
```
if (macroParameters.critRolls.enabled && macroParameters.critRolls.success(diceResult, result, DC)) {
  check = {status: "Critical Success", verb: "critically succeeds", color: "#C1EDA5"}
} else if (macroParameters.critRolls.enabled && macroParameters.critRolls.failure(diceResult, result, DC)) {
  check = {status: "Critical Failure", verb: "critically fails", color: "#EDA5A5"}
} else if (macroParameters.checkFunction(result, DC)) {
  check = {status: "Success", verb: "succeeds", color: "#C1EDA5"}
} else {
  check = {status: "Failure", verb: "fails", color: "#EDA5A5"}
}
content = `<div style="background-color: ${check.color}">${check.status} ! ${token.actor.data.name} ${check.verb} their ${skillName}'s 
check with a result of ${result} (${roll.result}) against a DC of ${DC}</div>`
```
  - the `check` object contains three attributes : 
    - `status` : the first word that is printed in the result
    - `verb` : the verb in use in the sentence
    - `color` : the background color used
  - the `content` variable contains the basic structure of the sentence. You can change it however you would like !
  You just need to respect the use of backquotes (\` \`) and the use of `${ }` to display variables 

You should do that for every Attribute Group you want to launch skill checks on. 

## Setting the DC

Currently, this script is meant to test against a DC that is the skill value itself : you can find that here ->

```
const DC = macroParameters.specifyDC ? parseInt(html.find('[name="DC"]')[0].value) : (macroParameters.DC !=0) ? macroParameters.DC : selectedOption.value
```

You can change the DC to be against a target quite easily by using things like `game.user.targets[0].actor.data.<whatever>` (assumes
that there is only one target)

You can also specify the DC for each roll by setting the `macroParameter.DC` to `0` and `macroParameter.specifyDC` to `true`


## License
Parts of this code are loosely inspired from a PF2e macro from u/Griff4218

This macro is distributed with the [GNU GPLv3 License](https://www.gnu.org/licenses/gpl-3.0.en.html) : you can freely modify it and change 
it, you can redistribute your modifications, but you have to do it under the same license, and you have to keep attribution.