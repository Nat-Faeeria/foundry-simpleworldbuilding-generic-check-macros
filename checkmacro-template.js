const macroParameters = {
	diceToRoll: "d20",
	category: "skills",
	checkName: "Skill",
	checkFunction: (roll, DC) => roll>=DC,
	critRolls: {
		enabled: true, 
		success: (roll, total, DC) => roll==20 || total >= DC+10,
		failure: (roll, total, DC) => roll==1 || total<=DC-10
	},
	DC: 0,
	specifyDC: false,
	bonusToRoll: true,
	bonusFunction: "+"
}

let chatData = {
	type: CHAT_MESSAGE_TYPES.ROLL,
	user: game.user.id,
	content: "",
	speaker: ChatMessage.getSpeaker(),
	roll: null
}

let content = "";

if (token) {

	const category = token.actor.data.data.attributes[macroParameters.category]

	let options = '';

	for (let element in category) {
	element = category[element]
	options += `<option value='${element.value}'>${element.label}</option>`;
	}

	const bonusSection = macroParameters.bonusToRoll ? `
	<div class="form-group">
		<label for="bonus">Check bonus :</label>
		<input type="number" id="bonus" name="bonus" />
	</div>` : "";

	const DCSection = macroParameters.specifyDC ? `
	<div class="form-group">
		<label for="DC">Check DC :</label>
		<input type="number" id="DC" name="DC" />
	</div>` : "";

	let applyChanges = false;

	new Dialog({
		title: `${macroParameters.checkName} check`,
		content: `
		<div>Choose the ${macroParameters.checkName} to test:<div>
		<hr/>
		<form>
			<div class="form-group">
				<label for="${macroParameters.checkName}">${macroParameters.checkName} :</label>
				<select id="${macroParameters.checkName}" name="${macroParameters.checkName}">
					`+options+`
				</select>
			</div>
			${bonusSection}	
			${DCSection}		  
		</form>
		`,
		buttons: {
		yes: {
			icon: "<i class='fas fa-check'></i>",
			label: `Throw`,
			callback: () => applyChanges = true
		},
		no: {
			icon: "<i class='fas fa-times'></i>",
			label: `Cancel`,
			allback: () => applyChanges = false
		},
		},
		default: "yes",
		close: html => {
			if (applyChanges) {
				let check = {status: "", verb: "", color: ""}
				const selectedOption = html.find(`[name="${macroParameters.checkName}"]`)[0].selectedOptions[0]
				const DC = macroParameters.specifyDC ? parseInt(html.find('[name="DC"]')[0].value) : (macroParameters.DC !=0) ? macroParameters.DC : parseInt(selectedOption.value)
				const skillName = selectedOption.text || "Skill"
				
				let diceToRoll = macroParameters.diceToRoll
				if (macroParameters.bonusToRoll) {
					const bonus = html.find('[name="bonus"]')[0].value || "0"
					if (bonus != "0") { diceToRoll += macroParameters.bonusFunction + bonus }
				}
				let roll = new Roll(diceToRoll).roll();
				const diceResult = roll.result.split("+")[0];
				let result = roll.total <=0 ? 1 : roll.total

				if (macroParameters.critRolls.enabled && macroParameters.critRolls.success(diceResult, result, DC)) {
					check = {status: "Critical Success", verb: "critically succeeds", color: "#C1EDA5"}
				} else if (macroParameters.critRolls.enabled && macroParameters.critRolls.failure(diceResult, result, DC)) {
					check = {status: "Critical Failure", verb: "critically fails", color: "#EDA5A5"}
				} else if (macroParameters.checkFunction(result, DC)) {
					check = {status: "Success", verb: "succeeds", color: "#C1EDA5"}
				} else {
					check = {status: "Failure", verb: "fails", color: "#EDA5A5"}
				}
				content = `<div style="background-color: ${check.color}">${check.status} ! ${token.actor.data.name} ${check.verb} their ${skillName}'s 
				check with a result of ${result} (${roll.result}) against a DC of ${DC}</div>`
				chatData.content = content
				chatData.roll = roll
				ChatMessage.create(chatData, {})
			}
		}
	}).render(true);
} else {
	ui.notifications.error("No token selected, please select the token that will use this action");
}